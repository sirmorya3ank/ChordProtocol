import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.DurationInt
import scala.util.Random

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Cancellable
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.event.LoggingReceive
import scala.util.control._

object ChordBonus {

def main(args: Array[String]){

	  if(args.length != 2) {
	  	System.exit(1)
	  }
	  val numberOfNodes = args(0).toInt;
	  val numberOfRequests = args(1).toInt;
	  val system= ActorSystem("Chord")
	  val watcher = system.actorOf(Props(new Watcher(numberOfNodes, numberOfRequests)), name="Watcher")
	  watcher ! Watcher.StartThread
	}

	object Watcher {
		case object StartThread
		case object JoinFinished
		case object PopulateSuccList
		case object KillNodes
		case object StartRouting
		case class tempFunc(n: ActorRef) 
		case class RoutingResponse(hopCount: Int)
		
	}

	class Watcher(numberOfNodes: Int, 
				numberOfRequests: Int) extends Actor {
		import Watcher._
		import context._
		var nodesArray = ArrayBuffer.empty[ActorRef]
		var joinCount : Int = 0
		var totalHopCount : Int = 0
		var requests : Int = 0
		var parentNode = actorOf(Props(new Node()), name = "Node0")
		var m = scala.math.ceil(scala.math.log(1.0 * numberOfNodes)/scala.math.log(2.0)).toInt
		var failureRate: Double = 0.25
		final def receive = {
			case StartThread =>
				parentNode ! Node.join(parentNode, self, m)
				for(i <- 1 to numberOfNodes-1) {
					var newNode: ActorRef = actorOf(Props(new Node()), name = "Node" + i)
					newNode ! Node.join(parentNode, self, m)
				}

				
			case tempFunc(n: ActorRef) =>
				//println("")
				n !  Node.PrintFingerTable()
				//n ! Node.PrintSuccessorList()

			case JoinFinished =>
				nodesArray += sender
				joinCount += 1
				println("Join Finished of "+ sender.path.name.drop(4).toInt + "Present Count of : "+ joinCount )
				if(joinCount == numberOfNodes){
					println("Total joins finished.")
					for(i <- 0 to nodesArray.length-1) {

						//system.scheduler.schedule(2 seconds, 30  milliseconds, nodesArray(i), Node.fixFingers())
						
						
						//self ! tempFunc(nodesArray(i))
					
					}

					for(i <- 0 to nodesArray.length-1) {

												
						//system.scheduler.schedule(0 seconds, 1 seconds, self, tempFunc(nodesArray(i)))
						//self ! tempFunc(nodesArray(i))
					
					}						
					
					// wait for 1 sec
					system.scheduler.scheduleOnce(1 seconds, self, Watcher.PopulateSuccList)

					//self ! 	StartRouting		
				}

			case PopulateSuccList =>
				
				for(i <- 0 to nodesArray.length-1) {
					//println("Started populating successor list of " + nodesArray(i))
					nodesArray(i) ! Node.populateSuccessorList(new Message(0, nodesArray(i)))
				}
				for(i <- 0 to nodesArray.length-1) {
											
						system.scheduler.schedule(0 seconds, 1 seconds, self, tempFunc(nodesArray(i)))
						//self ! tempFunc(nodesArray(i))
					
				}
				// wait for 1 sec
				system.scheduler.scheduleOnce(1 seconds, self, Watcher.KillNodes)
				//self ! killNodes

			case KillNodes =>
				println("Started killing nodes ")
				var totalFailedNodes:Int = scala.math.ceil(failureRate * numberOfNodes).toInt
				for(i <- 1 to totalFailedNodes) {
					var index: Int = scala.util.Random.nextInt(numberOfNodes)
					println("Node killed "+ nodesArray(index))
					nodesArray(index) ! Node.killNode()
				}
				// wait for 1 sec
				//system.scheduler.scheduleOnce(1 seconds, self, Watcher.StartRouting)
				//self ! StartRouting

			case StartRouting =>
				println("Started Routing")
				for(i <- 0 to nodesArray.length - 1){
					//println("Routing Started For : " +  nodesArray(i))
					nodesArray(i) ! Node.startRouting(numberOfRequests)
					//system.scheduler.scheduleOnce( 10 * i seconds, nodesArray(i), Node.startRouting(numberOfRequests))		
				}

			case RoutingResponse(hopCount: Int) =>
				//println(hopCount+" hops for searching ")
				totalHopCount += hopCount
				requests += 1
				if(requests == (numberOfNodes * numberOfRequests)){
					println("Average Hop Count is : "+ (1.0 * totalHopCount)/requests)
				        context.system.shutdown
				}
		}

	}

	object Node {

		case class join(existingNode: ActorRef, watcher: ActorRef, m : Int)
		case class joinReply(succVal: ActorRef) 
		case class stablize()
		case class fixFingers()
		case class fetchPredecessor()
		case class fetchPredecessorReply(predecessor: ActorRef)
		case class notifySuccessor(nDash: ActorRef)
		case class fixFingersSuccessorReply(successorVal: ActorRef, msg : Message)
		case class PrintFingerTable()
		case class startRouting(numberOfRequests: Int)
		case class startRequest()
		case class startRequestReply(msg: Message)
		case class findSuccessor(msg: Message)
		case class findSuccessorReply(msg: Message, node: ActorRef)
		case class findPredecessor(msg: Message, node: ActorRef)
		case class findPredecessorReply(msg: Message, node: ActorRef)
		case class getOwnSuccessor(msg: Message)
		case class getOwnSuccessorReply(msg: Message, node: ActorRef)
		case class findClosestPrecedingFinger(msg: Message)

		case class populateSuccessorList(msg: Message)
		case class populateSuccessorListReply(msg: Message)
		case class successorCheck()
		case class checkIsAlive(index: Int)
		case class checkIsAliveReply(index: Int, isAliveStatus: Boolean)
		case class killNode()
		case class PrintSuccessorList()
		case class stablizeReply()
		case class isPredAlive(sourceNode: ActorRef) 

case class fixFingersSuccessorReplyPart1(successorVal: ActorRef, msg : Message) 
		case class fixFingersSuccessorReplyPart1FetchPred(msg : Message)
	}

	class Node extends Actor{
		import Node._
		import context._

		var m: Int = 0
		var successorList = ArrayBuffer.empty[ActorRef]
		var isAlive: Boolean = true
		var watcherRef: ActorRef = null
		var fingerTable = ArrayBuffer.empty[ActorRef]
		var succListIndex: Int = 0
		var nDash: ActorRef = null
		var fixFingersIndex : Int = 1
		var predecessor : ActorRef = null

		def receive = LoggingReceive {

			case PrintFingerTable() =>
				var s= getNodeNumber(self)+":"
				for(i <- 0 to fingerTable.length-1){
					s= s+" "+getNodeNumber(fingerTable(i))
					//println(getNodeNumber(self) + " *****=====> " +getNodeNumber(fingerTable(i)))
				}
				//s= s+" pred-"+predecessor
				println(s)

			case PrintSuccessorList() =>
				var s= getNodeNumber(self)+":"
				for(i <- 0 to successorList.length-1){
					s= s+" "+getNodeNumber(successorList(i))
				}
				println(s)

			case join(existingNode: ActorRef, watcher: ActorRef , mVal : Int) =>
					m = mVal
					watcherRef = watcher
					if(existingNode == self){
						
						self ! joinReply(self)
					}else{
						var msg: Message = new Message(getNodeNumber(self), self)
						msg.joinMode = 1
							//println(getNodeNumber(self) + " is joined")
						existingNode ! findSuccessor(msg)
						//existingNode ! findSuccessor(msg)
					}

			/*case findSuccessorReply( successorVal: ActorRef ) =>	
					fingerTable(0) = successorVal
					self ! joinReply()*/

			case joinReply(succVal: ActorRef) =>
					fingerTable += succVal
					//println("Join is called")
					//system.scheduler.scheduleOnce( 1 seconds, self, Node.stablize())		
					for(i <- 1 to 5){
						//system.scheduler.scheduleOnce( 10 * i seconds, self, Node.stablize())		
					}
					system.scheduler.schedule(0 seconds, 10 milliseconds, self, Node.stablize())
					system.scheduler.schedule(1 seconds, 30  milliseconds, self, Node.fixFingers())		
					watcherRef ! Watcher.JoinFinished
			
			case stablize() =>
					if(successorList.length == 0){
						//println("Successor length = 0. Calling stablizeReply")
						self ! stablizeReply()
					}else{
						self ! successorCheck() 
					}

			case stablizeReply() =>
					//println(" Stablize of "+ getNodeNumber(self) + " and successor is "+ getNodeNumber(fingerTable(0)))
					fingerTable(0) ! fetchPredecessor()

			case fetchPredecessor() =>
					//println("In fetch predecessor of "+ getNodeNumber(self) + " and predecessor is "+ predecessor )
					//sender ! fetchPredecessorReply(predecessor)
					if(predecessor == null)
						sender ! fetchPredecessorReply(self)
					else 
						predecessor ! isPredAlive(sender)

			case isPredAlive(sourceNode: ActorRef) =>
					if(isAlive)
						sourceNode ! fetchPredecessorReply(self)		
			

			case fetchPredecessorReply(predecessorVal: ActorRef) =>
					//println("Find predecessor reply" + getNodeNumber(predecessorVal) + " in self "+ getNodeNumber(self) +" and succ is"+ getNodeNumber(fingerTable(0)) )
					if(belongsTo(getNodeNumber(predecessorVal), getNodeNumber(self) + 1, getNodeNumber(fingerTable(0)))){
						fingerTable(0) = predecessorVal
					}
					fingerTable(0) ! Node.notifySuccessor(self)

			case notifySuccessor(nDash: ActorRef) =>
					//println("In notifySuccessor predecessor = " + predecessor + " in self "+ getNodeNumber(self) +" and succ is"+ getNodeNumber(fingerTable(0)) )
					if(predecessor == null || belongsTo(getNodeNumber(nDash), getNodeNumber(predecessor) + 1, getNodeNumber(self) -1)){
						predecessor = nDash
}





			case fixFingersSuccessorReplyPart1(successorVal: ActorRef, msg : Message) =>
					//println(getNodeNumber(self)+" finding succ  of "+msg.id+" and getting "+getNodeNumber(successorVal))
					self ! fixFingersSuccessorReply(successorVal, msg)
					//successorVal ! 	fixFingersSuccessorReplyPart1FetchPred(msg)

			case fixFingersSuccessorReplyPart1FetchPred( msg : Message) =>
					sender ! fixFingersSuccessorReply(predecessor, msg)


			case fixFingers() =>
				//println("fixing fingers of "+getNodeNumber(self))
					if(fixFingersIndex >= m){
						fixFingersIndex = 1
					}
					//QueryMode 1: Find Successor while fixing Fingers
					var msg: Message = new Message((scala.math.pow(2, fixFingersIndex).toInt + getNodeNumber(self))%scala.math.pow(2, m).toInt, self)
					msg.fixFingerIndex = fixFingersIndex
					//println("finger index for msg "+msg.fixFingerIndex)
					//QueryMode 0: Find Successor while joining
					msg.joinMode = 2

					self ! findSuccessor(msg)

			case fixFingersSuccessorReply(successorVal: ActorRef, msg : Message) =>
				//println("getting the succ for fix fingers")
					if(fingerTable.length > msg.fixFingerIndex)
						fingerTable(msg.fixFingerIndex) = successorVal
					else
						fingerTable += successorVal
					fixFingersIndex += 1 
					//self ! fixFingers()

			case startRouting(numberOfRequests: Int) =>
				//println("tick "+getNodeNumber(self))
				for(i <- 1 to numberOfRequests){
					system.scheduler.scheduleOnce( 1 * i seconds, self, Node.startRequest())		
				}

			case startRequest() =>
				var maxNodes: Int = scala.math.pow(2, m).toInt
				var key: Int = scala.util.Random.nextInt(maxNodes)
				//var key: Int = 31
				var msg: Message = new Message(key, self)
				self ! findSuccessor(msg)

			case findSuccessor(msg: Message) =>
				//println("Search is for "+ msg.id + " by "+ getNodeNumber(msg.sourceNode)+ " in "+ getNodeNumber(self) )
				msg.sourceNode = self
				self ! findPredecessor(msg, self)

			case findPredecessor(msg: Message, node: ActorRef) =>
				nDash = node
				msg.ownSuccQueryMode = 1
				nDash ! getOwnSuccessor(msg)

			case getOwnSuccessor(msg: Message) =>
				msg.sourceNode ! getOwnSuccessorReply(msg, fingerTable(0))

			case getOwnSuccessorReply(msg: Message, nDashSucc: ActorRef) =>
				if(msg.ownSuccQueryMode == 1) {
					if(!belongsTo(msg.id, getNodeNumber(nDash)+1, getNodeNumber(nDashSucc))) {
						if(getNodeNumber(nDash) != getNodeNumber(self)) {
							msg.hopCount = msg.hopCount + 1
							msg.path += getNodeNumber(nDash)
						}
							
						nDash ! findClosestPrecedingFinger(msg)
					}
					else {
						self ! findPredecessorReply(msg, nDash)
					}
				}
				else if(msg.ownSuccQueryMode == 2) {
					self ! findSuccessorReply(msg, nDashSucc)
				}

			case findClosestPrecedingFinger(msg: Message) => 
				var closestPrecFingerNode: ActorRef = findClosestPrecedingFingerImpl(msg.id)
				msg.sourceNode ! findPredecessor(msg, closestPrecFingerNode)

			case findPredecessorReply(msg: Message, node: ActorRef) =>
				msg.ownSuccQueryMode = 2
				node ! getOwnSuccessor(msg)

			case findSuccessorReply(msg: Message, node: ActorRef) =>
				//println("Successor for "+msg.id  +" is "+getNodeNumber(node))
				if(msg.joinMode == 0)
					self ! startRequestReply(msg)
				else if(msg.joinMode == 1) {
					//println("Calling Join Reply")
					msg.callingNode ! joinReply(node)
				}
				else if(msg.joinMode == 2) 
					self ! fixFingersSuccessorReplyPart1(node, msg)

			case populateSuccessorList(msg: Message) =>
				if(msg.succList.length == m) {
					msg.sourceNode ! populateSuccessorListReply(msg)
				}
				else {
					msg.succList += fingerTable(0)
					fingerTable(0) ! populateSuccessorList(msg)
				}

			case populateSuccessorListReply(msg: Message) =>
				successorList = ArrayBuffer.empty[ActorRef]
				successorList ++= msg.succList
				//println("successor list population finished for node "+getNodeNumber(self))

			case successorCheck() =>
				succListIndex = 0
				successorList(0) ! checkIsAlive(succListIndex)

			case checkIsAlive(index: Int) =>
				sender ! checkIsAliveReply(index, isAlive)

			case checkIsAliveReply(index: Int, isAliveStatus:Boolean) =>
				if(!isAliveStatus) {
					succListIndex = succListIndex+1
					if(index == m) {
						//println("succ list size exceeded for node "+getNodeNumber(self)+". Check code for bugs")
						println("Something is Wrong!!!")
						context.system.shutdown
					}
					successorList(succListIndex) ! checkIsAlive(succListIndex)
				}else {
					if(succListIndex > 0) {
						
						fingerTable(0) = successorList(succListIndex)
						println("Successor set to "+getNodeNumber(fingerTable(0)) +" in self "+ getNodeNumber(self))
						self ! populateSuccessorList(new Message(0, self))
					}else if(index == 0){
						self ! stablizeReply()
					}
				
				}

			case killNode() =>
				isAlive = false

			case startRequestReply(msg: Message) =>
				if(msg.hopCount > m) {
					println(msg.hopCount+" hops for searching "+msg.id+" in "+getNodeNumber(self))
					println("path is "+getNodeNumber(self)+" - "+msg.path)
					context.system.shutdown
				}
				watcherRef ! Watcher.RoutingResponse(msg.hopCount)
		}

		def belongsTo(t: Int, s: Int, e: Int) : Boolean = {
	     	var totalIdval: Int  = scala.math.pow(2, m).toInt
	     	var t1: Int = (t+totalIdval)%totalIdval
	     	var s1: Int = (s+totalIdval)%totalIdval
	     	var e1: Int = (e+totalIdval)%totalIdval
	     	if(e1>=s1) {
	     		if(t1>=s1 && t1<=e1)
	     			return true
	     		else 
	     			return false
	     	}
	     	else {
	     		if(t1<s1 && t1>e1)
	     			return false
	     		else
	     			return true
	     	}
	    }

	    def findClosestPrecedingFingerImpl(id: Int): ActorRef = {
	    	var nodeNumber: Int = getNodeNumber(self)
	    	var fingerTableNodeNumber: Int = 0
	    	var maxFingerTableLength:Int = fingerTable.length
	    	for(count <- maxFingerTableLength-1 until (-1,-1)) {
	    		fingerTableNodeNumber = getNodeNumber(fingerTable(count))
	    		if(belongsTo(fingerTableNodeNumber, nodeNumber+1, id-1)) {
	    			return fingerTable(count)
	    		}
	    	}
	    	return self
	    }

		def getNodeNumber(node: ActorRef): Int = {
	    	return node.path.name.drop(4).toInt
	    }

	}

	class Message(keyId: Int, caller: ActorRef) {
		var id: Int = keyId
		var sourceNode: ActorRef = caller
		var callingNode: ActorRef = caller
		var hopCount: Int = 0
		var ownSuccQueryMode: Int = 0
		var joinMode: Int = 0
		var succList = ArrayBuffer.empty[ActorRef]
		var fixFingerIndex: Int = 0
		var path = ArrayBuffer.empty[Int]
	}
}
